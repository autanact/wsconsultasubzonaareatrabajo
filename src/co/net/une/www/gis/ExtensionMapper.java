
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package co.net.une.www.gis;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString50".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString50.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "UTCDate".equals(typeName)){
                   
                            return  co.net.une.www.gis.UTCDate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString100".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString100.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString14".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString14.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedLatitud".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedLatitud.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedLongitud".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedLongitud.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WsConsultarSubzonaAreaTrabajo-RQ-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsConsultarSubzonaAreaTrabajoRQType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedNumber9".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedNumber9.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString8".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString8.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString6".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString6.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "boundedString2".equals(typeName)){
                   
                            return  co.net.une.www.gis.BoundedString2.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "WsConsultarSubzonaAreaTrabajo-RS-Type".equals(typeName)){
                   
                            return  co.net.une.www.gis.WsConsultarSubzonaAreaTrabajoRSType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.une.net.co/gis".equals(namespaceURI) &&
                  "GisRespuestaGeneralType".equals(typeName)){
                   
                            return  co.net.une.www.gis.GisRespuestaGeneralType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    