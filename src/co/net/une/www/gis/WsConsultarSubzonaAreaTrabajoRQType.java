
/**
 * WsConsultarSubzonaAreaTrabajoRQType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.gis;
            

            /**
            *  WsConsultarSubzonaAreaTrabajoRQType bean class
            */
        
        public  class WsConsultarSubzonaAreaTrabajoRQType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = WsConsultarSubzonaAreaTrabajo-RQ-Type
                Namespace URI = http://www.une.net.co/gis
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/gis")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for IdSolicitud
                        */

                        
                                    protected co.net.une.www.gis.BoundedNumber9 localIdSolicitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedNumber9
                           */
                           public  co.net.une.www.gis.BoundedNumber9 getIdSolicitud(){
                               return localIdSolicitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IdSolicitud
                               */
                               public void setIdSolicitud(co.net.une.www.gis.BoundedNumber9 param){
                            
                                            this.localIdSolicitud=param;
                                    

                               }
                            

                        /**
                        * field for Latitud
                        */

                        
                                    protected co.net.une.www.gis.BoundedLatitud localLatitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedLatitud
                           */
                           public  co.net.une.www.gis.BoundedLatitud getLatitud(){
                               return localLatitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Latitud
                               */
                               public void setLatitud(co.net.une.www.gis.BoundedLatitud param){
                            
                                            this.localLatitud=param;
                                    

                               }
                            

                        /**
                        * field for Longitud
                        */

                        
                                    protected co.net.une.www.gis.BoundedLongitud localLongitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedLongitud
                           */
                           public  co.net.une.www.gis.BoundedLongitud getLongitud(){
                               return localLongitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Longitud
                               */
                               public void setLongitud(co.net.une.www.gis.BoundedLongitud param){
                            
                                            this.localLongitud=param;
                                    

                               }
                            

                        /**
                        * field for CodigoDepartamento
                        */

                        
                                    protected co.net.une.www.gis.BoundedString2 localCodigoDepartamento ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString2
                           */
                           public  co.net.une.www.gis.BoundedString2 getCodigoDepartamento(){
                               return localCodigoDepartamento;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoDepartamento
                               */
                               public void setCodigoDepartamento(co.net.une.www.gis.BoundedString2 param){
                            
                                            this.localCodigoDepartamento=param;
                                    

                               }
                            

                        /**
                        * field for CodigoMunicipio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString8 localCodigoMunicipio ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString8
                           */
                           public  co.net.une.www.gis.BoundedString8 getCodigoMunicipio(){
                               return localCodigoMunicipio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoMunicipio
                               */
                               public void setCodigoMunicipio(co.net.une.www.gis.BoundedString8 param){
                            
                                            this.localCodigoMunicipio=param;
                                    

                               }
                            

                        /**
                        * field for CodigoBarrio
                        */

                        
                                    protected co.net.une.www.gis.BoundedString6 localCodigoBarrio ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoBarrioTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString6
                           */
                           public  co.net.une.www.gis.BoundedString6 getCodigoBarrio(){
                               return localCodigoBarrio;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoBarrio
                               */
                               public void setCodigoBarrio(co.net.une.www.gis.BoundedString6 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoBarrioTracker = true;
                                       } else {
                                          localCodigoBarrioTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoBarrio=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       WsConsultarSubzonaAreaTrabajoRQType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/gis");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":WsConsultarSubzonaAreaTrabajo-RQ-Type",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "WsConsultarSubzonaAreaTrabajo-RQ-Type",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localIdSolicitud==null){
                                                 throw new org.apache.axis2.databinding.ADBException("IdSolicitud cannot be null!!");
                                            }
                                           localIdSolicitud.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","IdSolicitud"),
                                               factory,xmlWriter);
                                        
                                            if (localLatitud==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Latitud cannot be null!!");
                                            }
                                           localLatitud.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Latitud"),
                                               factory,xmlWriter);
                                        
                                            if (localLongitud==null){
                                                 throw new org.apache.axis2.databinding.ADBException("Longitud cannot be null!!");
                                            }
                                           localLongitud.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","Longitud"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoDepartamento==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoDepartamento cannot be null!!");
                                            }
                                           localCodigoDepartamento.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDepartamento"),
                                               factory,xmlWriter);
                                        
                                            if (localCodigoMunicipio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoMunicipio cannot be null!!");
                                            }
                                           localCodigoMunicipio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoMunicipio"),
                                               factory,xmlWriter);
                                         if (localCodigoBarrioTracker){
                                            if (localCodigoBarrio==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoBarrio cannot be null!!");
                                            }
                                           localCodigoBarrio.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoBarrio"),
                                               factory,xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "IdSolicitud"));
                            
                            
                                    if (localIdSolicitud==null){
                                         throw new org.apache.axis2.databinding.ADBException("IdSolicitud cannot be null!!");
                                    }
                                    elementList.add(localIdSolicitud);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Latitud"));
                            
                            
                                    if (localLatitud==null){
                                         throw new org.apache.axis2.databinding.ADBException("Latitud cannot be null!!");
                                    }
                                    elementList.add(localLatitud);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "Longitud"));
                            
                            
                                    if (localLongitud==null){
                                         throw new org.apache.axis2.databinding.ADBException("Longitud cannot be null!!");
                                    }
                                    elementList.add(localLongitud);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoDepartamento"));
                            
                            
                                    if (localCodigoDepartamento==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoDepartamento cannot be null!!");
                                    }
                                    elementList.add(localCodigoDepartamento);
                                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoMunicipio"));
                            
                            
                                    if (localCodigoMunicipio==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoMunicipio cannot be null!!");
                                    }
                                    elementList.add(localCodigoMunicipio);
                                 if (localCodigoBarrioTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoBarrio"));
                            
                            
                                    if (localCodigoBarrio==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoBarrio cannot be null!!");
                                    }
                                    elementList.add(localCodigoBarrio);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static WsConsultarSubzonaAreaTrabajoRQType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            WsConsultarSubzonaAreaTrabajoRQType object =
                new WsConsultarSubzonaAreaTrabajoRQType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"WsConsultarSubzonaAreaTrabajo-RQ-Type".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsConsultarSubzonaAreaTrabajoRQType)co.net.une.www.gis.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","IdSolicitud").equals(reader.getName())){
                                
                                                object.setIdSolicitud(co.net.une.www.gis.BoundedNumber9.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Latitud").equals(reader.getName())){
                                
                                                object.setLatitud(co.net.une.www.gis.BoundedLatitud.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","Longitud").equals(reader.getName())){
                                
                                                object.setLongitud(co.net.une.www.gis.BoundedLongitud.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoDepartamento").equals(reader.getName())){
                                
                                                object.setCodigoDepartamento(co.net.une.www.gis.BoundedString2.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoMunicipio").equals(reader.getName())){
                                
                                                object.setCodigoMunicipio(co.net.une.www.gis.BoundedString8.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoBarrio").equals(reader.getName())){
                                
                                                object.setCodigoBarrio(co.net.une.www.gis.BoundedString6.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          