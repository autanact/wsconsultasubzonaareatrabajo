
/**
 * WsConsultarSubzonaAreaTrabajoRSType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package co.net.une.www.gis;
            

            /**
            *  WsConsultarSubzonaAreaTrabajoRSType bean class
            */
        
        public  class WsConsultarSubzonaAreaTrabajoRSType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = WsConsultarSubzonaAreaTrabajo-RS-Type
                Namespace URI = http://www.une.net.co/gis
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://www.une.net.co/gis")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for IdSolicitud
                        */

                        
                                    protected co.net.une.www.gis.BoundedNumber9 localIdSolicitud ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedNumber9
                           */
                           public  co.net.une.www.gis.BoundedNumber9 getIdSolicitud(){
                               return localIdSolicitud;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IdSolicitud
                               */
                               public void setIdSolicitud(co.net.une.www.gis.BoundedNumber9 param){
                            
                                            this.localIdSolicitud=param;
                                    

                               }
                            

                        /**
                        * field for CodigoSubzona
                        */

                        
                                    protected co.net.une.www.gis.BoundedString50 localCodigoSubzona ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoSubzonaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString50
                           */
                           public  co.net.une.www.gis.BoundedString50 getCodigoSubzona(){
                               return localCodigoSubzona;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoSubzona
                               */
                               public void setCodigoSubzona(co.net.une.www.gis.BoundedString50 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoSubzonaTracker = true;
                                       } else {
                                          localCodigoSubzonaTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoSubzona=param;
                                    

                               }
                            

                        /**
                        * field for NombreSubzona
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreSubzona ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreSubzonaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreSubzona(){
                               return localNombreSubzona;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreSubzona
                               */
                               public void setNombreSubzona(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreSubzonaTracker = true;
                                       } else {
                                          localNombreSubzonaTracker = false;
                                              
                                       }
                                   
                                            this.localNombreSubzona=param;
                                    

                               }
                            

                        /**
                        * field for CodigoAreaTrabajo
                        */

                        
                                    protected co.net.une.www.gis.BoundedString50 localCodigoAreaTrabajo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCodigoAreaTrabajoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString50
                           */
                           public  co.net.une.www.gis.BoundedString50 getCodigoAreaTrabajo(){
                               return localCodigoAreaTrabajo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CodigoAreaTrabajo
                               */
                               public void setCodigoAreaTrabajo(co.net.une.www.gis.BoundedString50 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localCodigoAreaTrabajoTracker = true;
                                       } else {
                                          localCodigoAreaTrabajoTracker = false;
                                              
                                       }
                                   
                                            this.localCodigoAreaTrabajo=param;
                                    

                               }
                            

                        /**
                        * field for NombreAreaTrabajo
                        */

                        
                                    protected co.net.une.www.gis.BoundedString100 localNombreAreaTrabajo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNombreAreaTrabajoTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.BoundedString100
                           */
                           public  co.net.une.www.gis.BoundedString100 getNombreAreaTrabajo(){
                               return localNombreAreaTrabajo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NombreAreaTrabajo
                               */
                               public void setNombreAreaTrabajo(co.net.une.www.gis.BoundedString100 param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localNombreAreaTrabajoTracker = true;
                                       } else {
                                          localNombreAreaTrabajoTracker = false;
                                              
                                       }
                                   
                                            this.localNombreAreaTrabajo=param;
                                    

                               }
                            

                        /**
                        * field for GisRespuestaProceso
                        */

                        
                                    protected co.net.une.www.gis.GisRespuestaGeneralType localGisRespuestaProceso ;
                                

                           /**
                           * Auto generated getter method
                           * @return co.net.une.www.gis.GisRespuestaGeneralType
                           */
                           public  co.net.une.www.gis.GisRespuestaGeneralType getGisRespuestaProceso(){
                               return localGisRespuestaProceso;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GisRespuestaProceso
                               */
                               public void setGisRespuestaProceso(co.net.une.www.gis.GisRespuestaGeneralType param){
                            
                                            this.localGisRespuestaProceso=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       WsConsultarSubzonaAreaTrabajoRSType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://www.une.net.co/gis");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":WsConsultarSubzonaAreaTrabajo-RS-Type",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "WsConsultarSubzonaAreaTrabajo-RS-Type",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localIdSolicitud==null){
                                                 throw new org.apache.axis2.databinding.ADBException("IdSolicitud cannot be null!!");
                                            }
                                           localIdSolicitud.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","IdSolicitud"),
                                               factory,xmlWriter);
                                         if (localCodigoSubzonaTracker){
                                            if (localCodigoSubzona==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoSubzona cannot be null!!");
                                            }
                                           localCodigoSubzona.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoSubzona"),
                                               factory,xmlWriter);
                                        } if (localNombreSubzonaTracker){
                                            if (localNombreSubzona==null){
                                                 throw new org.apache.axis2.databinding.ADBException("NombreSubzona cannot be null!!");
                                            }
                                           localNombreSubzona.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreSubzona"),
                                               factory,xmlWriter);
                                        } if (localCodigoAreaTrabajoTracker){
                                            if (localCodigoAreaTrabajo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("CodigoAreaTrabajo cannot be null!!");
                                            }
                                           localCodigoAreaTrabajo.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoAreaTrabajo"),
                                               factory,xmlWriter);
                                        } if (localNombreAreaTrabajoTracker){
                                            if (localNombreAreaTrabajo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("NombreAreaTrabajo cannot be null!!");
                                            }
                                           localNombreAreaTrabajo.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreAreaTrabajo"),
                                               factory,xmlWriter);
                                        }
                                            if (localGisRespuestaProceso==null){
                                                 throw new org.apache.axis2.databinding.ADBException("GisRespuestaProceso cannot be null!!");
                                            }
                                           localGisRespuestaProceso.serialize(new javax.xml.namespace.QName("http://www.une.net.co/gis","GisRespuestaProceso"),
                                               factory,xmlWriter);
                                        
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "IdSolicitud"));
                            
                            
                                    if (localIdSolicitud==null){
                                         throw new org.apache.axis2.databinding.ADBException("IdSolicitud cannot be null!!");
                                    }
                                    elementList.add(localIdSolicitud);
                                 if (localCodigoSubzonaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoSubzona"));
                            
                            
                                    if (localCodigoSubzona==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoSubzona cannot be null!!");
                                    }
                                    elementList.add(localCodigoSubzona);
                                } if (localNombreSubzonaTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "NombreSubzona"));
                            
                            
                                    if (localNombreSubzona==null){
                                         throw new org.apache.axis2.databinding.ADBException("NombreSubzona cannot be null!!");
                                    }
                                    elementList.add(localNombreSubzona);
                                } if (localCodigoAreaTrabajoTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "CodigoAreaTrabajo"));
                            
                            
                                    if (localCodigoAreaTrabajo==null){
                                         throw new org.apache.axis2.databinding.ADBException("CodigoAreaTrabajo cannot be null!!");
                                    }
                                    elementList.add(localCodigoAreaTrabajo);
                                } if (localNombreAreaTrabajoTracker){
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "NombreAreaTrabajo"));
                            
                            
                                    if (localNombreAreaTrabajo==null){
                                         throw new org.apache.axis2.databinding.ADBException("NombreAreaTrabajo cannot be null!!");
                                    }
                                    elementList.add(localNombreAreaTrabajo);
                                }
                            elementList.add(new javax.xml.namespace.QName("http://www.une.net.co/gis",
                                                                      "GisRespuestaProceso"));
                            
                            
                                    if (localGisRespuestaProceso==null){
                                         throw new org.apache.axis2.databinding.ADBException("GisRespuestaProceso cannot be null!!");
                                    }
                                    elementList.add(localGisRespuestaProceso);
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static WsConsultarSubzonaAreaTrabajoRSType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            WsConsultarSubzonaAreaTrabajoRSType object =
                new WsConsultarSubzonaAreaTrabajoRSType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"WsConsultarSubzonaAreaTrabajo-RS-Type".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (WsConsultarSubzonaAreaTrabajoRSType)co.net.une.www.gis.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","IdSolicitud").equals(reader.getName())){
                                
                                                object.setIdSolicitud(co.net.une.www.gis.BoundedNumber9.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoSubzona").equals(reader.getName())){
                                
                                                object.setCodigoSubzona(co.net.une.www.gis.BoundedString50.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreSubzona").equals(reader.getName())){
                                
                                                object.setNombreSubzona(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","CodigoAreaTrabajo").equals(reader.getName())){
                                
                                                object.setCodigoAreaTrabajo(co.net.une.www.gis.BoundedString50.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","NombreAreaTrabajo").equals(reader.getName())){
                                
                                                object.setNombreAreaTrabajo(co.net.une.www.gis.BoundedString100.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.une.net.co/gis","GisRespuestaProceso").equals(reader.getName())){
                                
                                                object.setGisRespuestaProceso(co.net.une.www.gis.GisRespuestaGeneralType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          