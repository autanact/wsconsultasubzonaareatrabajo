
/**
 * WsConsultarSubzonaAreaTrabajoServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsConsultarSubzonaAreaTrabajoServiceSkeleton java skeleton for the axisService
     */
    public class WsConsultarSubzonaAreaTrabajoServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsConsultarSubzonaAreaTrabajoServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param idSolicitud
                                     * @param latitud
                                     * @param longitud
                                     * @param codigoDepartamento
                                     * @param codigoMunicipio
                                     * @param codigoBarrio
         */
        

                 public co.net.une.www.gis.WsConsultarSubzonaAreaTrabajoRSType consultaSubzonaAreaTrabajo
                  (
                  co.net.une.www.gis.BoundedNumber9 idSolicitud,co.net.une.www.gis.BoundedLatitud latitud,co.net.une.www.gis.BoundedLongitud longitud,co.net.une.www.gis.BoundedString2 codigoDepartamento,co.net.une.www.gis.BoundedString8 codigoMunicipio,co.net.une.www.gis.BoundedString6 codigoBarrio
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("idSolicitud",idSolicitud);params.put("latitud",latitud);params.put("longitud",longitud);params.put("codigoDepartamento",codigoDepartamento);params.put("codigoMunicipio",codigoMunicipio);params.put("codigoBarrio",codigoBarrio);
		try{
		
			return (co.net.une.www.gis.WsConsultarSubzonaAreaTrabajoRSType)
			this.makeStructuredRequest(serviceName, "consultaSubzonaAreaTrabajo", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    